#!/bin/bash
docker build -t linc-toil:test Docker

USER=lofarsys

docker run --net=host --rm \
  -v /data/software/slurm/v20.11.9/:/opt/slurm:ro \
  -v /etc/slurm:/opt/slurm/etc:ro \
  -v /etc/munge:/etc/munge:ro \
  -v /var/run/munge:/var/run/munge \
  --user=`id -u $USER`:`id -g $USER` \
  -v /etc/passwd:/etc/passwd:ro \
  -v /etc/group:/etc/group:ro \
  -e USER -e HOME \
  -e PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/home/mol/miniconda3/bin \
  -v /home/mol/miniconda3:/home/mol/miniconda3:ro \
  -v /data:/data:rw \
  -v /var/run/docker.sock:/var/run/docker.sock\
   linc-toil:test /run-linc.sh /git/LINC/workflows/HBA_calibrator.cwl /HBA_calibrator.json
