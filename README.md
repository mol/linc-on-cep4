Prerequisites:

* a modern docker (that supports --mount) → this is available on cpu46-50 as part of the "testing" partition (see SDCH-3183),
* be able to use docker on the nodes you're going to use (be part of the "docker" group),
* run `install_deps.sh` in all nodes that will execute job steps. This will install python3, and the required pip packages.

Then, to run LINC, run either:

* `build-and-run-docker.sh` to spawn the main job as well as its steps in Docker,
* `build-and-run-native.sh` to spawn the main job natively but its steps in Docker.

Both will run on CEP4 using cached input in `/data/home/mol`, and output to `/data/tmp/toil/12345/`.

The pipeline runs from the Docker image supplied in this repo, or natively, and spawns steps on CEP4 using SLURM (in the "testing" partition). These steps need a bit of toil installed, hence the `install_deps.sh` on each node. For users with a shared homedir, this is not needed as the installation on any node will cause the software to be available on all of them.
