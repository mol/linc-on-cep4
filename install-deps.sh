#!/bin/bash

# Install Python3
[ -r Miniconda3-latest-Linux-x86_64.sh ] || wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O Miniconda3-latest-Linux-x86_64.sh
[ -d ~/miniconda3 ] || sh ./Miniconda3-latest-Linux-x86_64.sh -b
~/miniconda3/bin/conda init
source ~/.bashrc

[ -d LINC ] || git clone https://git.astron.nl/RD/LINC.git

# Install python3 packages
pip3 install --upgrade -r requirements.txt
