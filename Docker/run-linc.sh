#!/bin/bash
export PATH=$PATH:/opt/slurm/bin
TMSS_ID=12345

TOIL_OUTPUT_DIR=/data/tmp/toil/$TMSS_ID

rm -rf "${TOIL_OUTPUT_DIR}"
mkdir -p "${TOIL_OUTPUT_DIR}"/{logs,workDir,tmpOutput}

TOIL_SLURM_ARGS="--partition cpu --comment=$TMSS_ID" \
  toil-cwl-runner \
    --jobStore "${TOIL_OUTPUT_DIR}/jobStore" \
    --workDir "${TOIL_OUTPUT_DIR}/workDir/" \
    --batchSystem slurm \
    --bypass-file-store \
    --preserve-entire-environment \
    --disableProgress \
    --disableCaching \
    --retryCount 3 \
    --logDebug --logFile "${TOIL_OUTPUT_DIR}/logs/toil-cwl-runner.log" \
    --writeLogs "${TOIL_OUTPUT_DIR}/logs/" \
    --writeLogsFromAllJobs True \
    --tmp-outdir-prefix "${TOIL_OUTPUT_DIR}/tmpOutput/output-" \
    --outdir "${TOIL_OUTPUT_DIR}/output/" \
    "$@"
